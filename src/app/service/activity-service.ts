import { Observable } from "rxjs";
import { Activity } from "../entity/activity";

export abstract class ActivityService {
    abstract getActivitys() : Observable<Activity[]>;
}
