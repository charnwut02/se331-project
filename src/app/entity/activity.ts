export class Activity {
    actId:number;
    actName: string;
    actLocation: string;
    actDescription: string;
    actPeriod: string;
    actDate: Date;
    actHost: string;
}
